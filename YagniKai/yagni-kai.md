%title: mdp - Yagni Kai
%author: James A. Sral
%date: 08-28-2019

# YAGNI Kai
## Fighting for Simplicity and Honor!

---

-> # Bio
  James is a husband, Father of one (with another on the way), and drinker
  of much coffee. He works at LoadUp Technologies, using Rails and all three
  major frameworks. He also is a classically trained guitarist and likes to mess
  around with game engines in his seconds of spare time.

---

-> # Warning - Opinions Ahead!
  - These opinions are my own and are shaped by my personal experience
  - I prefer doing interactive talks. These talking points are guidelines
  - Interrupt me to ask questions (seriously)
  - I only want to talk about what you are interested in talking about

---

-> # It's not about always saying "No"
  - Some of these tools are very helpful, some are harmful (IMHO), and some
    are neutral and depend entirely on the situation/team
  - This talk was started with a presentation program. It crashed.
    Then I decided YAGNI! And so here we are with Markdown

---

-> # Premature Optimization vs Premature Abstraction
  - "Premature Optimization is the root of all evil" - Someone
  - What does this even mean? Optimizing for what?
  - Many diminsions of optimization - Speed, Readability, Flexability, Dope-ness
  - Ex. TTFB, TTI, SEO, ALAXCVJF (ok I made that one up)

---

-> # Solve the Problems You Have Right Now
  - "Duplication is far cheaper than the wrong abstraction." - Sandi Metz
  - "DRY is very overrated" - Me, others probably
  - Abstractions can't be preempted ->  Yagni!
  - You'll never know less than you do right now
  - Not an excuse for ignoring requirements, but you can evaluate/question them

---

-> # You Ain't Gonna Need It
  - Like “DRY”, it’s a guideline, not an implementation
  - Like any guideline, it’s possible to take things too far. Use your best judgment
  - Solve the problems you have, not the ones you think you’ll have

---

-> # Layers of Abstraction
  - Architecture & Tooling
  - Code Patterns & Techniques
  - Team Processes & Communication

---

-> # Architecture
  - Typescript? Elm? Babel? Just Javascript(tm)?
  - Build Tools - Webpack? Parcel? Gulp? Makefile?
  - Frameworks or Vanilla?
  - Server-Rendering? SPA? Isomorphic (a la Next/Nuxt)? Sprinkles (a la StimulusJS)?
  - MicroServices? Serverless? Monolith?

---

-> # Code Patterns & Techniques
  - Design Patterns (Strategy, Factory, Observer, Visitor, etc)
  - RxJS? Redux? MobX?
  - Functional? Object-Oriented? Procedural?
  - Containers? Injection? HoCs? Render Props? Hooks?
  - Ex. Prop Spreading ->  YAGNI!

---

-> # Team Processes and Communication
  - Company Wiki
  - Linting - Opinionated? Un-Opinionated?
  - Formatters (Standard/Prettier)
  - Merge Requirements - Approvals, Code Review

---

-> # Questions?

---

-> # Thanks, y'all!
